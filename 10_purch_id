
%macro purch_id4(pref =, period =, jan =);

proc sql stimer;
	%connect_db(&db, libname = y, dbmstemp = yes);
option compress = yes;
%act_id;

/*****************************/
*BULKLOAD
/*****************************/
*Step 1-1 : Load JAN data;
CREATE TABLE &db..&bulk_pref.jan (bulkload = yes dbtype = (upc_cd = 'bigint') bl_options = "logdir &logdir.") AS
SELECT
	DISTINCT grp_nbr, upc_cd

FROM
	&jan.

/*WHERE*/
/*	grp_nbr IN (&grp_nbr.)*/
;
/****************************************************/
				*Get Purchase Data;
/****************************************************/
*Step 2-1 : Get purchase data;
EXECUTE
(
	CREATE TEMP TABLE purch_id AS
		SELECT
			o.cal_dt,
			o.pid_key,
			l.parent_corp_key,
			j.grp_nbr,
			o.tran_nbr,
			SUM(o.purch_amt) 			AS amt,
			SUM(o.purch_qty) 			AS qty

		FROM
			order_upc_v 				AS o
			INNER JOIN location_v 		AS l ON o.cmc_chn_str_nbr = l.cmc_chn_str_nbr
			INNER JOIN act_id_flt 		AS i ON o.pid_key = i.pid_key AND l.parent_corp_key = i.parent_corp_key
			INNER JOIN &bulk_pref.jan 	AS j ON o.upc_cd = j.upc_cd

		WHERE
			o.cal_dt BETWEEN &&st_dt_P&period. AND &&ed_dt_P&period.
			AND o.pid_key > 0
			AND o.purch_amt > 0
			AND o.purch_qty > 0
			AND o.cmc_chn_nbr IN (&chain_no.)
			AND %common_criteria(o)

		GROUP BY
			1,2,3,4,5

	DISTRIBUTE ON RANDOM
) by &db.
;

EXECUTE
(
	CREATE TEMP TABLE ota_id AS
		SELECT
			pid_key,
			parent_corp_key,
			SUM(amt) 					AS amt,
			SUM(qty) 					AS qty,
			COUNT(DISTINCT tran_nbr)	AS tran

		FROM
			purch_id

		WHERE
			cal_dt BETWEEN &st_dt_P5. AND &ed_dt_P5.
			AND grp_nbr = 2

		GROUP BY 
			1,2
	DISTRIBUTE ON RANDOM
) by &db.
;

EXECUTE
(
	CREATE TEMP TABLE pre_id AS
		SELECT
			pid_key,
			parent_corp_key,
			SUM(amt) 					AS amt,
			SUM(qty) 					AS qty,
			COUNT(DISTINCT tran_nbr)	AS tran

		FROM
			purch_id

		WHERE
			cal_dt BETWEEN &st_dt_P4 AND &ed_dt_P4

		GROUP BY 
			1,2
	DISTRIBUTE ON RANDOM
) by &db.
;

EXECUTE
(
	CREATE TEMP TABLE newex_id_flg AS
		SELECT
			o.pid_key,
			o.parent_corp_key,

			CASE
			WHEN p.pid_key IS NOT NULL THEN 1
			ELSE 0
			END AS pre_flg,

			o.amt			AS amt,
			o.qty			AS qty,
			o.tran			AS tran,
			NVL(p.amt,0)	AS amt_pre,
			NVL(p.qty,0)	AS qty_pre,
			NVL(p.tran,0)	AS tran_pre

		FROM
			ota_id 				AS o
			LEFT JOIN pre_id	AS p ON o.pid_key = p.pid_key AND o.parent_corp_key = p.parent_corp_key

	DISTRIBUTE ON RANDOM
) by &db.		
;


EXECUTE
(
	CREATE TEMP TABLE newex_id AS
		SELECT
			pre_flg,
			COUNT(DISTINCT parent_corp_key||'_'||pid_key) 	AS id_cnt,
			SUM(amt) 										AS amt,
			SUM(qty) 										AS qty,
			SUM(tran)										AS tran,
			SUM(amt_pre) 									AS amt_pre,
			SUM(qty_pre) 									AS qty_pre,
			SUM(tran_pre)									AS tran_pre

		FROM
			newex_id_flg

		GROUP BY 
			1
	DISTRIBUTE ON RANDOM
) by &db.		
;

		*Step 2-1-1 : Create new exist data output;
		CREATE TABLE tmp.newex_id AS
		SELECT *
		FROM &db..newex_id  
		;

EXECUTE
(
	CREATE TEMP TABLE newex_rep_id_flg AS
		SELECT
			n.pid_key,
			n.parent_corp_key,
			n.pre_flg,

			CASE
			WHEN r.pid_key IS NOT NULL THEN 1
			ELSE 0
			END AS rep_flg,

			n.amt			AS amt,
			n.qty			AS qty,
			n.tran			AS tran,
			NVL(r.amt,0)	AS amt_rep,
			NVL(r.qty,0)	AS qty_rep,
			NVL(r.tran,0)	AS tran_rep

		FROM
			newex_id 			AS n
			INNER JOIN purch_id 	AS r ON n.pid_key = r.pid_key AND n.parent_corp_key = r.parent_corp_key

		WHERE
			r.tran > 1

	DISTRIBUTE ON RANDOM
) by &db.
;

EXECUTE
(
	CREATE TEMP TABLE newex_rep_id AS
		SELECT
			pre_flg,
			rep_flg,
			COUNT(DISTINCT parent_corp_key||'_'||pid_key) 	AS id_cnt,
			SUM(amt) 										AS amt,
			SUM(qty) 										AS qty,
			SUM(tran)										AS tran,
			SUM(amt_rep) 									AS amt_rep,
			SUM(qty_rep) 									AS qty_rep,
			SUM(tran_rep)									AS tran_rep

		FROM
			newex_rep_id_flg

		GROUP BY
			1,2

	DISTRIBUTE ON RANDOM
) by &db.
;

		*Step 2-1-1 : Create new exist repeat data output;
		CREATE TABLE tmp.newex_rep_id AS
		SELECT *
		FROM &db..newex_rep_id  
		;

quit;
%mend;

/*purch Macro*/
/*%let grp_nbr = 1,2;*/
%purch_id4(pref = all, 	period = 7, jan = imp_jan);
